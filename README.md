ReadMe


Try.py is the result of the first task assigned by the Multunus team. 
The program takes the information and can output it in text and pdf format


final.py is the final output for the problem provided. The program accepts the movie details, 
stores it in a dictionary, and can convert it to pdf or text format.
The conversion capability can be expanded by copying the new files in the plugin directory.
The plugin can have any name, but the name of the plugin will be displayed on the menu, so it is suggested to 
follow the format- "Convert2_Formatname" In the file, the class must be named Convert and the function inside 
it must be named run The plugins must accept the dictionary containing the movie details as argument.


How the program works: In all of the conversions, the dictionary containing the movie details is passed
1. Conversion to text: The content from the dictionary is read in pairs(key and value) and combined into a string which is written to a text file using the write function. 
2. Conversion to pdf: The reportlab module is used for pdf conversion. A string is created by reading the key and value from 
	the dictionary which is then written into the canvas using the drawstring function at the coordinates 10,800-x, where the
	value of x is incremented after each loop
3. I believe the rest of the conversions can be carried out in similar fashion, by using the contents of the dictionary.

The plugin feature: The program uses the pkgutil module to access and load the
plugins to the program. pkgutil.itermodule provides us with all the moduless found 
in the plugins folder, by using the getattr function,we obtain the filepath which is used 
to load the particlar plugin