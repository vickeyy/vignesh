import os
from reportlab.pdfgen import canvas
from time import sleep

class movie_database:
	def insert_data(self):
		movie={"Title":raw_input("Title of the movie: "),
				"Runtime":raw_input("Runtime:"),
				"Language":raw_input("Language:"),
				"Male Lead":raw_input("Male Lead: "),
				"Female Lead":raw_input("Female Lead: "),
				"Genre":raw_input("Genre: ")}
		return movie
	def menu(self):
		clearscreen()
		menu_string="\n0.Exit\n1.Text\n2.Pdf\n"
		return raw_input(menu_string)
	def text(self,movie):
		f=open("output.txt","w")
		for key in movie:
			string=key+":"+movie[key]+"\n"
			f.write(string)
	def pdf(self,movie):
		c = canvas.Canvas("output.pdf")
		x=0
		for key in movie:
			string=key+":"+movie[key]
			c.drawString(10,800-x,string)
			x=x+20
		c.showPage()
		c.save()
	def convert(self,movie):
		choice=-1
		while True:
			choice=self.menu()
			choice=int(choice)
			if choice==0:
				exit()
			elif choice==1:
				self.text(movie)
			elif choice==2:
				self.pdf(movie)
			else:
				"\nInvalid input\n"
			clearscreen()
			sleep(1)
			print "Your file is ready"
			sleep(2)
			clearscreen()

    	
def clearscreen():
    os.system('clear')
    titlebar()

def titlebar():
    print"\n\t\t**********MOVIE_DATABASE**********\t\t\n"   		

if __name__=="__main__":
	clearscreen()
	movie=movie_database()
	dict=movie.insert_data()	
	movie.convert(dict)

