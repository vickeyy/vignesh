import pkgutil
import sys
import os
from time import sleep
from reportlab.pdfgen import canvas

class movie_database:
    """The main class which contains the functions to accept the details and convert it to different formats"""

    def insert_data(self):
        """Function to input the data"""
        self.movie={"Title":raw_input("Title of the movie: "),
                "Runtime":raw_input("Runtime:"),
                "Language":raw_input("Language:"),
                "Male Lead":raw_input("Male Lead: "),
                "Female Lead":raw_input("Female Lead: "),
                "Genre":raw_input("Genre: ")}
        return movie




    def dynamic_menu(self):
        """Function to extend the menu as the plugins are added"""
        path = os.path.join(os.path.dirname(__file__), "plugins")
        modules = pkgutil.iter_modules(path=[path])
        string=""
        count=3
        self.mod_no={}
        for loader, mod_name, ispkg in modules:
            # Ensure that module isn't already loaded
            if mod_name not in sys.modules:
                loaded_mod = __import__(path+"."+mod_name,fromlist=[mod_name])
                #the class in the plugin must be always named Convert
                class_name = "Convert"
                #if plugin is added, load it else ignore
                try:
                    #loaded_contains the path of the class which comprises is filepath.classname
                    loaded_class = getattr(loaded_mod, class_name)
                except:
                    pass
            #if a new plugin is added, add it to the menu, else ignore""
            try:
                string=string+str(count)+"."+mod_name+"\n"
                
            except:
                pass
            self.mod_no[str(count)]=loaded_class
            #menu index
            count=count+1

        return string

    def dynamic_choose(self,choice):
        """Function to call the function from the plugin"""
        load=self.mod_no[str(choice)]
        instance = load()
        #Run is the default function to be implemented in all the plugins, and the dictionary movie is passed
        instance.run(self.movie)

    def menu(self):
        """Function to generate the menu"""
        clearscreen()
        menu_string="\n0.Go back\n1.Convert2_text\n2.Convert2_Pdf\n"+self.dynamic_menu()
        return raw_input(menu_string)
    
    def text(self,movie):
        """Function to convert the output to txt format"""
        file=open("output.txt","w")
        for key in self.movie:
            string=key+":"+self.movie[key]+"\n"
            file.write(string)
    
    def pdf(self,movie):
        """Function to convert the output to pdf format"""
        c = canvas.Canvas("output.pdf")
        x=0
        for key in self.movie:
            string=key+":"+self.movie[key]
            c.drawString(10,800-x,string)
            x=x+20
        c.showPage()
        c.save()
    
    def convert(self,movie):
        """Function to handle the conversion choices"""
        choice=-2
        while True:
            choice=self.menu()
            try:
                choice=int(choice)
                if choice==0:
                    clearscreen()
                    sleep(1)
                    break
                elif choice==1:
                    clearscreen()
                    self.text(movie)
                elif choice==2:
                    clearscreen()
                    self.pdf(movie)
                elif choice>=3:
                    clearscreen()
                    sleep(1)
                    self.dynamic_choose(str(choice))
                sleep(2)
                clearscreen()
                print "Your file is ready"
                sleep(1)
                clearscreen()
            except:
                pass
                






def clearscreen():
    os.system('clear')
    titlebar()

def titlebar():
    print"\n\t\t**********MOVIE_DATABASE**********\t\t\n"           



if __name__=="__main__":
    clearscreen()
    movie=movie_database()
    choice=0
    while True:
            choice=(raw_input("\n\t1.New Record\n\t2.Exit\n"))
            try:
                choice=int(choice)
                if choice==1:
                    clearscreen()
                    dict=movie.insert_data()    
                    clearscreen()
                    movie.convert(dict)
                elif  choice==2:
                    clearscreen()
                    print "\n\tThank You\n"
                    sleep(2)
                    os.system('clear')
                    break
            except:
                clearscreen()










